var classMap = {
  table: 'table table-bordered table-striped',
}

var bindings =
    Object.keys(classMap).map(key => ({
                                type: 'output',
                                regex: new RegExp(`<${key}>`, 'g'),
                                replace: `<${key} class="${classMap[key]}">`
                              }));
var c = new showdown.Converter(
    {tables: true, extensions: [...bindings], noHeaderId: true});
$('document').ready(function() {
  $('.xs3p-doc').each(function(i, obj) {
    var indent = $(this).html().match('^\\n[\\t ]*');
    if (!(indent === null)) {
      normalized = $(this).html().replace(new RegExp(indent[0], 'gm'), '\n');
    } else {
      normalized = $(this).html();
    }
    console.log(normalized);
    $(this).html(c.makeHtml(normalized));
  })
});