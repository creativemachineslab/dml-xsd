# Using locally to preview changes
Make sure you have installed the xsltproc tool:

```
sudo apt-get install xsltproc
```

Using the xs3p stylesheet we transform the dml.xsd into our reference.

```
xsltproc xs3p.xsl dml.xsd > reference\dml_out.html
```

# Deploying

Any changes pushed to master will be built and deployed at https://creativemachineslab.gitlab.io/dml-xsd/